<%-- 
    Document   : ModificarDocente
    Created on : 21/03/2014, 08:11:14 AM
    Author     : Maick
--%>

<%@page import="Modelo.Docente"%>
<%@page import="Controlador.ControlDocente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <% 
            ControlDocente cd = new ControlDocente();
            Docente docente = new Docente();
            docente.setIdDocente("123");
            docente.setNombre("Miguel");
            docente.setClave("gdfgdf");
            docente.setEstado(true);
            cd.GuardarDocente(docente);
            Docente doc = cd.ConsultarDocente("123");
            doc.setNombre("Julio");
            cd.ModificarDocente(doc);
            out.println("Modificado");
        %>
    </body>
</html>

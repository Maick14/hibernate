<%-- 
    Document   : ejemplo
    Created on : 20/03/2014, 12:09:27 PM
    Author     : Maick
--%>

<%@page import="Modelo.Docente"%>
<%@page import="Controlador.ControlDocente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <% 
            ControlDocente cd = new ControlDocente();
            Docente d = new Docente();
            d = cd.ConsultarDocente("1047428324");
            cd.EliminarDocente(d.getIdDocente());
            out.println("Docente eliminado: " + d.getNombre());
        %>
    </body>
</html>

package Modelo;
// Generated 17/03/2014 10:59:53 AM by Hibernate Tools 3.2.1.GA


import java.util.HashSet;
import java.util.Set;

/**
 * Docente generated by hbm2java
 */
public class Docente  implements java.io.Serializable {


     private String idDocente;
     private String nombre;
     private String clave;
     private Boolean estado;
     private Set<Asignatura> asignaturas = new HashSet<Asignatura>(0);

    public Docente() {
        idDocente = "";
        nombre = "";
        clave = "";
        estado = true;
    }

	
    public Docente(String idDocente) {
        this.idDocente = idDocente;
    }
    public Docente(String idDocente, String nombre, String clave, Boolean estado, Set<Asignatura> asignaturas) {
       this.idDocente = idDocente;
       this.nombre = nombre;
       this.clave = clave;
       this.estado = estado;
       this.asignaturas = asignaturas;
    }
   
    public String getIdDocente() {
        return this.idDocente;
    }
    
    public void setIdDocente(String idDocente) {
        this.idDocente = idDocente;
    }
    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getClave() {
        return this.clave;
    }
    
    public void setClave(String clave) {
        this.clave = clave;
    }
    public Boolean getEstado() {
        return this.estado;
    }
    
    public void setEstado(Boolean estado) {
        this.estado = estado;
    }
    public Set<Asignatura> getAsignaturas() {
        return this.asignaturas;
    }
    
    public void setAsignaturas(Set<Asignatura> asignaturas) {
        this.asignaturas = asignaturas;
    }




}



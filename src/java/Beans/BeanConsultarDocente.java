/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import Controlador.ControlDocente;
import Modelo.Docente;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Named;

/**
 *
 * @author Maick
 */

@Named(value = "ConsultarDocente")
@SessionScoped
public class BeanConsultarDocente implements Serializable{

    /**
     * Creates a new instance of BeanConsultarDocente
     */
    public BeanConsultarDocente() {
    }
    
    private String identificacion;
    private static Docente docente;
    ControlDocente cd = new ControlDocente();

    
    public void AsignarDocente(){
        cd = new ControlDocente();
        docente = new Docente();
        docente = cd.ConsultarDocente(identificacion);
        System.out.println(docente.getIdDocente() + docente.getNombre());
    }
    
    public void EliminarDocente(){
        cd.EliminarDocente(identificacion);
    }    
    
    public void destroyWorld(ActionEvent actionEvent){  
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Confirmacion",  "Docente " + docente.getNombre() + " eliminado");  
        FacesContext.getCurrentInstance().addMessage(null, message);  
    }  

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String id) {
        this.identificacion = id;
    }

    public Docente getDocente() {
        return docente;
    }
}
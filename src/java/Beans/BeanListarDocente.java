/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import Controlador.ControlDocente;
import Modelo.Docente;
import java.util.ArrayList;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author Maick
 */

@Named(value = "ListarDocente")
@RequestScoped
public class BeanListarDocente {
    
   private ControlDocente cd = new ControlDocente();
   private ArrayList<Docente> lista = new ArrayList<>();
  
    public BeanListarDocente() {
        
    }
    
    public void IniciarLista(){
        this.setLista((ArrayList<Docente>)cd.ListarDocentes());
    }

    public ControlDocente getCd() {
        return cd;
    }

    public void setCd(ControlDocente cd) {
        this.cd = cd;
    }

    public ArrayList<Docente> getLista() {
        return lista;
    }

    public void setLista(ArrayList<Docente> lista) {
        this.lista = lista;
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import Controlador.ControlDocente;
import Modelo.Docente;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author Maick
 */
@Named(value = "RegistrarDocente")
@RequestScoped
public class BeanRegistrarDocente {

    /**
     * Creates a new instance of BeanRegistrarDocente
     */
    
    private Docente docente = new Docente();
    
    public BeanRegistrarDocente() {
        
    }
    
    public void Save(){
        ControlDocente cd = new ControlDocente();
        Docente doc = new Docente();
        doc.setIdDocente(docente.getIdDocente());
        doc.setNombre(docente.getNombre());
        doc.setClave(docente.getClave());
        doc.setEstado(true);
        cd.GuardarDocente(docente);
    }
    
    public Docente getDocente() {
        return docente;
    }

    public void setDocente(Docente docente) {
        this.docente = docente;
    }
}

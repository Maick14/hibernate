/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Docente;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Maick
 */
public class ControlDocente {
    
    Session session = null;
    
    public ControlDocente(){
        this.session = NewHibernateUtil.getSessionFactory().getCurrentSession();
    }
    
   public List<Docente> ListarDocentes() {
        List<Docente> docente = null;
        try {
            org.hibernate.Transaction tx = session.beginTransaction();
            Query q = session.createQuery("From Docente as docente");
            docente = (List<Docente>) q.list();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return docente;
    }
    
    
   
   public void GuardarDocente(Docente docente){
       Transaction transaction = null;
       try{
           transaction = session.beginTransaction();
           session.save(docente);
           transaction.commit();
       }catch(Exception e){
           e.printStackTrace();
       }
   }
     
    public  void  ModificarDocente(Docente d){
        Transaction transaction;
        try {
            transaction = session.beginTransaction();
            session.update(d);
            transaction.commit();
            session.close();
        } catch (Exception e) {

        }
    }
 
    public void Desactivar(String identificacion) {
        Transaction transaction;
        try {
            Docente d =  ConsultarDocente(identificacion);
            transaction = session.beginTransaction();
            d.setEstado(false);
            session.update(d);
            transaction.commit();
            session.close();
        } catch (Exception e) {
        }
    }
    
   public void Activar(Docente d) {
        Transaction transaction;
        try {
            transaction = session.beginTransaction();
            d.setEstado(true);
            session.update(d);
            transaction.commit();
            session.close();
        } catch (Exception e) {
        }
    }
    
 
    public void EliminarDocente(String identificacion) {
        Transaction transaction;
        try {
            transaction = session.beginTransaction();
            List employees = session.createQuery("from Docente where id_docente ='" + identificacion + "'").list();
            for (Iterator iterator = employees.iterator(); iterator.hasNext();) {
                Docente docente = (Docente) iterator.next();
                session.delete(docente);
                transaction.commit();
            }
            session.close();
        } catch (Exception e) {

        }
    }
    
      public Docente ConsultarDocente(String identificacion) {
        Transaction transaction;
        Docente docente = null;
        try {
            transaction = session.beginTransaction();
            List employees = session.createQuery("from Docente where id_docente ='" + identificacion + "'").list();
            for (Iterator iterator = employees.iterator(); iterator.hasNext();) {
                docente = (Docente) iterator.next();
                break;
            }
            transaction.commit();
            session.close();
        } catch (Exception e) {

        }finally{
            return docente;
        }
    }
    
    
    public  void  CambiarClave(String identificacion, String clave){
        Transaction transaction;
        try {
            transaction = session.beginTransaction();
            String queryString = "from Docente where id_docente ="+identificacion;
            Query query = session.createQuery(queryString);
            Docente docente=(Docente) query.uniqueResult();
            docente.setClave(clave);
            session.update(docente);
            transaction.commit();
        } catch (Exception e) {

        } finally {
            session.close();
        }
    }
}
